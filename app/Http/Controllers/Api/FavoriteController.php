<?php

namespace App\Http\Controllers\Api;

use App\Favorite;
use App\Property;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FavoriteController extends Controller
{
    public function index($id)
    {
        $property_ids = Favorite::where('user_id',$id)->get(['property_id']);
        $properties = Property::whereIn('id',$property_ids)->get();
        return response()->json($properties, 200);
    }
    public function store(Request $request)
    {
        $favorite= Favorite::create([
            'user_id' => $request->user_id,
            'property_id'=> $request->property_id
        ]);
        return response()->json($favorite, 200);
    }
}
