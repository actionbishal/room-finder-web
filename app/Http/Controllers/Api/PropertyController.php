<?php

namespace App\Http\Controllers\Api;

use App\Property;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class PropertyController extends Controller
{
    private $destinationPath = 'img/uploads/';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function userProperty($user_id)
    {
        $properties = Property::where('user_id',$user_id)->get();

        return response()->json($properties);
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$user_id)
    {
        $property = Property::create([
            'user_id' => $user_id,
            'city' => $request->city,
            'type' => $request->type
        ]);
//        $property->save();
//        $property->user()->attach($user_id);
//        $property->user()->associate(1);
        return response()->json($property->id, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Property::where('id',$id)->update([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
            'negotiable' => $request->negotiable,
            'preferred_tenants' => $request->preferredTenants,
            'furnishing' => $request->furnishing,
            'parking' => $request->parking,
            'contact'=>$request->contact,
            'locality' => $request->locality,
        ]);
        return response()->json($id, 200);
    }

    public function updateLocation(Request $request, $id)
    {
        Property::where('id',$id)->update([
            'locality' => $request->locality,
            'address' => $request->address,
            'landmark' => $request->landmark,
            'longitude' => $request->longitude,
            'latitude' => $request->latitude,
            'geocode' => $request->geocode
        ]);
        return response()->json($id, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function imageUpload(Request $request)
    {
        $fileName = $request->fileName;
        $property_id = $request->property;
        $imagePath = $this->destinationPath . $fileName;
        Input::file('file')->move($this->destinationPath, $fileName); // uploading file to given path
        Property::where('id',$property_id)->update([
            'image' => $imagePath
        ]);
    }

    public function getRecentProperties()
    {
        $properties = Property::orderBy('id','desc')->get();

        return response()->json($properties);
    }

    public function getFilteredProperties(Request $request)
    {
        $properties = Property::all();

        return response()->json($properties,200);
    }
}
