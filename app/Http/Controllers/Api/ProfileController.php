<?php

namespace App\Http\Controllers\Api;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class ProfileController extends Controller
{
    private $destinationPath = 'img/uploads/';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */

    public function getProfile($user_id)
    {
        return Profile::with('user')->where('user_id', $user_id)->get();
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user_id)
    {
        $profile = Profile::where('user_id',$user_id)->update([
            'about' => $request->about,
            'phone_no' => $request->phone_no
        ]);

        return $profile;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function imageUpdate(Request $request, $user_id)
    {
        $fileName = $request->fileName;
        $imagePath = $this->destinationPath . $fileName;
        Input::file('file')->move($this->destinationPath, $fileName); // uploading file to given path
        Profile::where('id',$user_id)->update([
            'image' => $imagePath
        ]);
    }
}
