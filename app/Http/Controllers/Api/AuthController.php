<?php

namespace App\Http\Controllers\Api;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        if(Auth::attempt(['email'=>$email,'password'=>$password])) {
            return User::where('id', Auth::user()->id)->with('profile')->first();
        };
    }

    public function register(Request $request)
    {
        $email = $request->email;
        $password = bcrypt($request->password);
        $user = User::create([
            'name' => $request->name,
            'email' => $email,
            'password' => $password
        ]);
        $user->profile()->save(new Profile);

        return  User::where('id', $user->id)->with('profile')->first();
    }
}
