<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    //
    protected $fillable = [
        'user_id', 'name', 'description', 'price', 'negotiable', 'parking', 'locality', 'city', 'type',
        'preferred-tenants', 'furnishing','contactnumber', 'geocode'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
