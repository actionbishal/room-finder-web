<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('user/{user_id}/property','Api\PropertyController@userProperty');
Route::get('get-recent-properties','Api\PropertyController@getRecentProperties');
Route::post('property/filter','Api\PropertyController@getFilteredProperties');
Route::post('property/store/{user_id}','Api\PropertyController@store');
Route::post('property/{id}/update','Api\PropertyController@update');
Route::post('property/{id}/update/location','Api\PropertyController@updateLocation');
Route::post('property/image/upload','Api\PropertyController@imageUpload');
Route::post('add-favorite','Api\FavoriteController@store');
Route::get('user/{id}/favorite','Api\FavoriteController@index');

Route::post('auth/login','Api\AuthController@login');
Route::post('auth/register','Api\AuthController@register');

Route::get('user/profile/{user_id}','Api\ProfileController@getProfile');
Route::post('user/profile/update/{user_id}','Api\ProfileController@update');
Route::post('user/profile/image/update/{user_id}','Api\ProfileController@imageUpdate');
