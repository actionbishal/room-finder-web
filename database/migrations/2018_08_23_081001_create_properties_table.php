<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->string('preferred_tenants')->nullable();
            $table->string('city')->nullable();
            $table->string('type')->nullable();
            $table->string('furnishing')->nullable();
            $table->string('image')->nullable();
            $table->string('geocode')->nullable();
            $table->string('locality')->nullable();
            $table->string('address')->nullable();
            $table->string('landmark')->nullable();
            $table->string('contact')->nullable();
            $table->decimal('longitude',10,7)->nullable();
            $table->decimal('latitude',10,7)->nullable();
            $table->string('parking')->nullable();
            $table->string('price')->nullable();
            $table->boolean('negotiable')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
